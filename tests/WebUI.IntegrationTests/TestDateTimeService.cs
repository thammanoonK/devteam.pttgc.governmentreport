﻿using PTTGC.GR.Report.Domain.Interfaces;
using System;

namespace PTTGC.GR.Report.WebUI.IntegrationTests
{
    public class TestDateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
