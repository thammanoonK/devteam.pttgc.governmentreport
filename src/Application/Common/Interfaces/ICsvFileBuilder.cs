﻿using PTTGC.GR.Report.Application.TodoItems.Queries.GetTodoItemsFile;
using System.Collections.Generic;

namespace PTTGC.GR.Report.Application.Common.Interfaces
{
    public interface ICsvFileBuilder
    {
        byte[] BuildTodoItemsFile(IEnumerable<TodoItemFileRecord> records);
    }
}
