﻿using PTTGC.GR.Report.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace PTTGC.GR.Report.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<TodoItem> TodoItems { get; set; }
        DbSet<ReportType> ReportTypes { get; set; }
        DbSet<DocumentInfo> DocumentInfos { get; set; }
        DbSet<UploadInfo> UploadInfos { get; set; }
        DbSet<TankMidnightMapping> TankMidnightMapping { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
