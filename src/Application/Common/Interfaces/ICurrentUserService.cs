﻿namespace PTTGC.GR.Report.Application.Common.Interfaces
{
    public interface ICurrentUserService
    {
        public string UserId { get; }
    }
}
