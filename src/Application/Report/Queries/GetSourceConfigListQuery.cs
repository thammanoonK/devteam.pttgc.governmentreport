﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using PTTGC.GR.Report.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;

namespace PTTGC.GR.Report.Application.Report
{
    public class GetSourceConfigListQuery : IRequest<SourceConfigList>
    {
        public long Id { get; set; }
        public class GetSourceConfigListQueryHandler : IRequestHandler<GetSourceConfigListQuery, SourceConfigList>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;
            private readonly IHostingEnvironment _hostingEnvironment;

            public GetSourceConfigListQueryHandler(IApplicationDbContext context, IMapper mapper, IHostingEnvironment hostingEnvironment)
            {
                _context = context;
                _mapper = mapper;
                _hostingEnvironment = hostingEnvironment;
            }

            public async Task<SourceConfigList> Handle(GetSourceConfigListQuery request, CancellationToken cancellationToken)
            {
                string contentRootPath = _hostingEnvironment.ContentRootPath;
                var json = System.IO.File.ReadAllText(contentRootPath + "/Config/ReportTypeConfig.json");
                var reportConfigModelList =  JsonConvert.DeserializeObject<List<ReportTypeConfig>>(json);
                var reportConfigModel = (from reportConfig in reportConfigModelList
                                         where reportConfig.Id == request.Id
                                         select reportConfig).FirstOrDefault();
                var vm = new SourceConfigList
                {
                    SourceFileConfigs = reportConfigModel.SourceFileConfigs
                };

                return vm;
            }
        }
    }
}
