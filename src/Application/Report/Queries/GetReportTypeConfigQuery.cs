﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using PTTGC.GR.Report.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using PTTGC.GR.Report.Application.Common.Exceptions;

namespace PTTGC.GR.Report.Application.Report
{
    public class GetReportTypeConfigQuery : IRequest<ReportTypeConfig>
    {
        public long Id { get; set; }
        public class GetReportTypeConfigQueryHandler : IRequestHandler<GetReportTypeConfigQuery, ReportTypeConfig>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;
            private readonly IHostingEnvironment _hostingEnvironment;

            public GetReportTypeConfigQueryHandler(IApplicationDbContext context, IMapper mapper, IHostingEnvironment hostingEnvironment)
            {
                _context = context;
                _mapper = mapper;
                _hostingEnvironment = hostingEnvironment;
            }

            public async Task<ReportTypeConfig> Handle(GetReportTypeConfigQuery request, CancellationToken cancellationToken)
            {
                //var items = await _context.ReportTypes
                //    .ProjectTo<ReportTypeDto>(_mapper.ConfigurationProvider)
                //    .ToListAsync(cancellationToken);
                string contentRootPath = _hostingEnvironment.ContentRootPath;
                var json = System.IO.File.ReadAllText(contentRootPath + "/Config/ReportTypeConfig.json");
                var reportTypeConfigList = JsonConvert.DeserializeObject<List<ReportTypeConfig>>(json);
                var vm = (from reportTypeConfig in reportTypeConfigList
                          where reportTypeConfig.Id == request.Id
                          select reportTypeConfig).FirstOrDefault();
                if (vm == null)
                {
                    throw new NotFoundException(nameof(ReportTypeConfig), request.Id);
                }

                return vm;
            }
        }
    }
}
