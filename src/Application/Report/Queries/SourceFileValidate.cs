﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using PTTGC.GR.Report.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using System;
using Microsoft.AspNetCore.Http;
using System.IO;
using OfficeOpenXml;

namespace PTTGC.GR.Report.Application.Report
{
    public class SourceFileValidate : IRequest<SourceConfigList>
    {
        public long Id { get; set; }
        public List<IFormFile> files { get; set; }
        public class SourceFileValidateHandler : IRequestHandler<SourceFileValidate, SourceConfigList>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;
            private readonly IHostingEnvironment _hostingEnvironment;

            public SourceFileValidateHandler(IApplicationDbContext context, IMapper mapper, IHostingEnvironment hostingEnvironment)
            {
                _context = context;
                _mapper = mapper;
                _hostingEnvironment = hostingEnvironment;
            }

            public async Task<SourceConfigList> Handle(SourceFileValidate request, CancellationToken cancellationToken)
            {
                string contentRootPath = _hostingEnvironment.ContentRootPath;
                var jsonData = System.IO.File.ReadAllText(contentRootPath + "/Config/ReportTypeConfig.json");
                var reportConfigModelList = JsonConvert.DeserializeObject<List<ReportTypeConfig>>(jsonData);
                var reportConfigModel = (from reportConfig in reportConfigModelList
                                         where reportConfig.Id == request.Id
                                         select reportConfig).FirstOrDefault();
                List<SourceConfig> sourceFileConfigList = new List<SourceConfig>();
                if (reportConfigModel != null)
                {
                    foreach (SourceConfig item in reportConfigModel.SourceFileConfigs)
                    {
                        sourceFileConfigList.Add(item);
                    }
                }
                var vm = new SourceConfigList
                {
                    SourceFileConfigs = new List<SourceConfig>()
                };
                try
                {
                    if (request.files.Count > 0)
                    {
                        foreach (IFormFile file in request.files)
                        {
                            foreach (SourceConfig sourceFileConfig in sourceFileConfigList)
                            {
                                if (sourceFileConfig.FileName == file.FileName)
                                {
                                    var json = System.IO.File.ReadAllText(contentRootPath + "/Config/ValidateConfig/" + sourceFileConfig.ValidateFile);
                                    var validateConfiglList = JsonConvert.DeserializeObject<List<ValidateConfig>>(json);
                                    SourceConfig viewModel = new SourceConfig();
                                    viewModel.FileName = file.FileName;
                                    viewModel.IsValid = true;
                                    viewModel.ValidMessage = "";
                                    using (var stream = new MemoryStream())
                                    {
                                        file.OpenReadStream().CopyTo(stream);
                                        using (var package = new ExcelPackage(stream))
                                        {
                                            foreach (ValidateConfig item in validateConfiglList)
                                            {
                                                ExcelWorksheet worksheet = package.Workbook.Worksheets[item.Name];
                                                if (worksheet != null)
                                                {
                                                    if (item.Validate != null)
                                                    {
                                                        bool exitLoop = false;
                                                        int rowCount = item.RowStart;
                                                        while (!exitLoop)
                                                        {
                                                            if (worksheet.Cells[rowCount, 2].Value == null && worksheet.Cells[rowCount, 3].Value == null && worksheet.Cells[rowCount, 5].Value == null && worksheet.Cells[rowCount, 7].Value == null && worksheet.Cells[rowCount, 11].Value == null)
                                                            {
                                                                exitLoop = true;
                                                                if (rowCount == item.RowStart)
                                                                {
                                                                    viewModel.IsValid = false;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                foreach (ValidateRule validateRule in item.Validate)
                                                                {
                                                                    var rowitem = worksheet.Row(rowCount);
                                                                    if (worksheet.Cells[rowCount, validateRule.Index].Value == null)
                                                                    {
                                                                        if (validateRule.Required)
                                                                        {
                                                                            viewModel.IsValid = false;
                                                                            viewModel.ValidMessage = viewModel.ValidMessage + "Required cell [" + rowCount + "," + validateRule.Index + "];" + System.Environment.NewLine;
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        var dataValue = worksheet.Cells[rowCount, validateRule.Index].Value;
                                                                        Type DataType = Type.GetType(validateRule.DataType);
                                                                        switch (DataType.Name)
                                                                        {
                                                                            case "DateTime":
                                                                                DateTime result;
                                                                                if (dataValue != null)
                                                                                {
                                                                                    bool isParse = DateTime.TryParse(dataValue.ToString(), out result);
                                                                                    if (!isParse)
                                                                                    {
                                                                                        viewModel.IsValid = false;

                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    viewModel.IsValid = false;
                                                                                }
                                                                                if (!viewModel.IsValid)
                                                                                {
                                                                                    viewModel.ValidMessage = viewModel.ValidMessage + "Required cell [" + rowCount + "," + validateRule.Index + " ] is DateTime;" + System.Environment.NewLine;
                                                                                }
                                                                                break;
                                                                            case "String":
                                                                                if (dataValue == null || dataValue.ToString().Trim() == "")
                                                                                {
                                                                                    viewModel.IsValid = false;
                                                                                    viewModel.ValidMessage = viewModel.ValidMessage + "Required cell is   [" + rowCount + "," + validateRule.Index + "] not empty;" + System.Environment.NewLine;
                                                                                }
                                                                                break;
                                                                            case "Decimal":
                                                                                decimal decimalRresult;
                                                                                if (dataValue != null)
                                                                                {
                                                                                    bool isParse = decimal.TryParse(dataValue.ToString(), out decimalRresult);
                                                                                    if (!isParse)
                                                                                    {
                                                                                        viewModel.IsValid = false;

                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    viewModel.IsValid = false;
                                                                                }
                                                                                if (!viewModel.IsValid)
                                                                                {
                                                                                    viewModel.ValidMessage = viewModel.ValidMessage + "Required cell [" + rowCount + "," + validateRule.Index + " ] is Decimal;" + System.Environment.NewLine;
                                                                                }
                                                                                break;

                                                                            default:
                                                                                Console.WriteLine("Default case");
                                                                                break;
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            rowCount = rowCount + 1;
                                                        }
                                                    }
                                                }
                                                else if (item.Required)
                                                {
                                                    viewModel.IsValid = false;
                                                    viewModel.ValidMessage = viewModel.ValidMessage + "Required Worksheet " + item.Name + ";" + System.Environment.NewLine;
                                                }

                                            }
                                            ////ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                                            //////ExcelWorksheet worksheet = package.Workbook.Worksheets["JAN"];
                                            ////var rowCount = worksheet.Dimension.Rows;

                                            ////for (int row = 2; row <= rowCount; row++)
                                            ////{

                                            ////}
                                        }
                                    }
                                    vm.SourceFileConfigs.Add(viewModel);
                                }
                            }
                        }
                    }
                    return vm;
                }
                catch (Exception ex)
                {
                    return vm;
                }
            }
        }
    }
}