﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using PTTGC.GR.Report.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;
using System;
using Microsoft.AspNetCore.Http;
using System.IO;
using OfficeOpenXml;

namespace PTTGC.GR.Report.Application.Report
{
    public class GenerateReport : IRequest<SourceConfig>
    {
        public long Id { get; set; }
        public string dateStr { get; set; }
        public List<IFormFile> files { get; set; }
        public class GenerateReportHandler : IRequestHandler<GenerateReport, SourceConfig>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;
            private readonly IHostingEnvironment _hostingEnvironment;
            public GenerateReportHandler(IApplicationDbContext context, IMapper mapper, IHostingEnvironment hostingEnvironment)
            {
                _context = context;
                _mapper = mapper;
                _hostingEnvironment = hostingEnvironment;
            }

            public async Task<SourceConfig> Handle(GenerateReport request, CancellationToken cancellationToken)
            {
                string[] _date = request.dateStr.Split("-");
                var vm = new SourceConfig();

                try
                {
                    string contentRootPath = _hostingEnvironment.ContentRootPath;
                    var jsonData = System.IO.File.ReadAllText(contentRootPath + "/Config/ReportTypeConfig.json");
                    var reportConfigModelList = JsonConvert.DeserializeObject<List<ReportTypeConfig>>(jsonData);
                    var reportConfigModel = (from reportConfig in reportConfigModelList
                                             where reportConfig.Id == request.Id
                                             select reportConfig).FirstOrDefault();
                    List<SourceConfig> sourceFileConfigList = new List<SourceConfig>();
                    if (reportConfigModel != null)
                    {
                        foreach (SourceConfig item in reportConfigModel.SourceFileConfigs)
                        {
                            sourceFileConfigList.Add(item);
                        }
                    }
                    FileInfo excel = new FileInfo(contentRootPath + "/Config/ReportTemplate/" + reportConfigModel.TemplateOutput);
                    using (var distinationPackage = new ExcelPackage(excel))
                    {
                        var workbook = distinationPackage.Workbook;
                        foreach (IFormFile file in request.files)
                        {
                            var json = System.IO.File.ReadAllText(contentRootPath + "/Config/CalculateConfig/" + reportConfigModel.CalculateFile);
                            var calculateConfiglList = JsonConvert.DeserializeObject<CalculateConfig>(json);

                            using (var stream = new MemoryStream())
                            {
                                file.OpenReadStream().CopyTo(stream);
                                using (var sourcePackage = new ExcelPackage(stream))
                                {
                                    if (calculateConfiglList != null)
                                    {
                                        foreach (CalculateRuleConfig ruleItem in calculateConfiglList.Source)
                                        {
                                            if (ruleItem.SourceFile == file.FileName)
                                            {
                                                ExcelWorksheet detailWorksheet = distinationPackage.Workbook.Worksheets["Detail"];
                                                ExcelWorksheet yearWorksheet = distinationPackage.Workbook.Worksheets["Year"];
                                                if (yearWorksheet != null)
                                                {
                                                    yearWorksheet.Cells[2, 3].Value = yearWorksheet.Cells[2, 3].Value + _date[0];
                                                    yearWorksheet.Cells[9, 3].Value = "01/01/" + _date[0];
                                                    yearWorksheet.Cells[10, 3].Value = "02/01/" + _date[0];
                                                    yearWorksheet.Cells[11, 3].Value = "03/01/" + _date[0];
                                                    yearWorksheet.Cells[12, 3].Value = "04/01/" + _date[0];
                                                    yearWorksheet.Cells[13, 3].Value = "05/01/" + _date[0];
                                                    yearWorksheet.Cells[14, 3].Value = "06/01/" + _date[0];
                                                    yearWorksheet.Cells[15, 3].Value = "07/01/" + _date[0];
                                                    yearWorksheet.Cells[16, 3].Value = "08/01/" + _date[0];
                                                    yearWorksheet.Cells[17, 3].Value = "09/01/" + _date[0];
                                                    yearWorksheet.Cells[18, 3].Value = "10/01/" + _date[0];
                                                    yearWorksheet.Cells[19, 3].Value = "11/01/" + _date[0];
                                                    yearWorksheet.Cells[20, 3].Value = "12/01/" + _date[0];
                                                }
                                                foreach (WorkSheetCalculateConfig workSheetItem in ruleItem.WorkSheet)
                                                {
                                                    ExcelWorksheet sourceWorksheet = sourcePackage.Workbook.Worksheets[workSheetItem.Source];
                                                    ExcelWorksheet destinationWorksheet = distinationPackage.Workbook.Worksheets[workSheetItem.Destination];
                                                    double amount = 0.0;
                                                    if (sourceWorksheet != null && destinationWorksheet != null)
                                                    {
                                                        int limitRow = 18;
                                                        int countRow = 1;
                                                        int itemCount = 1;
                                                        destinationWorksheet.Cells[3, 1].Value = destinationWorksheet.Cells[3, 1].Value + " " + _date[0];
                                                        int sourceRowCount = workSheetItem.SourceRowStart;
                                                        int desRowCount = workSheetItem.DestinationRowStart;
                                                        bool exitLoop = false;
                                                        while (!exitLoop)
                                                        {
                                                            destinationWorksheet.InsertRow(desRowCount, 1, desRowCount + 1);
                                                            if (sourceWorksheet.Cells[sourceRowCount, 2].Value == null && sourceWorksheet.Cells[sourceRowCount, 3].Value == null && sourceWorksheet.Cells[sourceRowCount, 5].Value == null && sourceWorksheet.Cells[sourceRowCount, 7].Value == null && sourceWorksheet.Cells[sourceRowCount, 11].Value == null)
                                                            {
                                                                exitLoop = true;
                                                            }
                                                            else
                                                            {
                                                                foreach (CellCalculateConfig cellItem in workSheetItem.Cell)
                                                                {
                                                                    if (cellItem.CalculateRule == "Assign")
                                                                    {
                                                                        destinationWorksheet.Cells[desRowCount, cellItem.DestinationCol].Value = sourceWorksheet.Cells[sourceRowCount, cellItem.SourceCol].Value;
                                                                        if (cellItem.DestinationCol == 5)
                                                                        {
                                                                            double tempOutput = 0.0;
                                                                            if (Double.TryParse(sourceWorksheet.Cells[sourceRowCount, cellItem.SourceCol].Value.ToString(), out tempOutput))
                                                                                amount = amount + tempOutput;
                                                                        };
                                                                    }
                                                                }
                                                            }
                                                            destinationWorksheet.Cells[desRowCount, 1].Value = itemCount;
                                                            destinationWorksheet.Row(desRowCount).Height = 24.0;
                                                            if (countRow == limitRow)
                                                            {
                                                                countRow = 0;
                                                                desRowCount = desRowCount + 11;
                                                                detailWorksheet.Cells[1, 1, 17, 6].Copy(destinationWorksheet.Cells[desRowCount, 1, desRowCount + 17, 6]);
                                                                //exitLoop = true;
                                                                desRowCount = desRowCount + 8;
                                                            }
                                                            else
                                                            {
                                                                desRowCount = desRowCount + 1;
                                                            }
                                                            sourceRowCount = sourceRowCount + 1;
                                                            countRow = countRow + 1;
                                                            itemCount = itemCount + 1;
                                                        }
                                                        destinationWorksheet.Cells["E" + (desRowCount + 1)].Formula = "=SUM(E" + workSheetItem.DestinationRowStart + ":E" + desRowCount + ")";
                                                    }
                                                    if (yearWorksheet != null)
                                                    {
                                                        switch (workSheetItem.Destination.ToLower())
                                                        {
                                                            case "jan":
                                                                yearWorksheet.Cells[9, 4].Value = amount;
                                                                break;
                                                            case "feb":
                                                                yearWorksheet.Cells[10, 4].Value = amount;
                                                                break;
                                                            case "mar":
                                                                yearWorksheet.Cells[11, 4].Value = amount;
                                                                break;
                                                            case "apr":
                                                                yearWorksheet.Cells[12, 4].Value = amount;
                                                                break;
                                                            case "may":
                                                                yearWorksheet.Cells[13, 4].Value = amount;
                                                                break;
                                                            case "jun":
                                                                yearWorksheet.Cells[14, 4].Value = amount;
                                                                break;
                                                            case "jul":
                                                                yearWorksheet.Cells[15, 4].Value = amount;
                                                                break;
                                                            case "aug":
                                                                yearWorksheet.Cells[16, 4].Value = amount;
                                                                break;
                                                            case "sep":
                                                                yearWorksheet.Cells[17, 4].Value = amount;
                                                                break;
                                                            case "oct":
                                                                yearWorksheet.Cells[18, 4].Value = amount;
                                                                break;
                                                            case "dec":
                                                                yearWorksheet.Cells[19, 4].Value = amount;
                                                                break;
                                                            case "nov":
                                                                yearWorksheet.Cells[20, 4].Value = amount;
                                                                break;
                                                            default:
                                                                Console.WriteLine("Default case");
                                                                break;
                                                        }

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        var fileName = reportConfigModel.DestinationFile + DateTime.Now.ToString("yyyy-MM-dd--hh-mm-ss") + ".xlsx";
                        var outputDir = contentRootPath + "/ReportFile/";
                        var distinationFile = new FileInfo(outputDir + fileName);
                        distinationPackage.SaveAs(distinationFile);
                        vm.FileName = fileName;
                        using (var outputStream = new MemoryStream())
                        {
                            distinationPackage.SaveAs(outputStream);
                            vm.Report = outputStream.ToArray();
                        }
                    }
                    vm.IsValid = true;

                    return vm;
                }
                catch (Exception ex)
                {
                    vm.IsValid = false;
                    vm.ValidMessage = ex.Message;
                    return vm;
                }
                return vm;
            }
        }
    }
}
