﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Application.Report
{
    public class CellCalculateConfig
    {
        public int SourceCol { get; set; }
        public int DestinationCol { get; set; }
        public string RowEnd { get; set; }
        public string CalculateRule { get; set; }
        public string Sumary { get; set; }
    }
}
