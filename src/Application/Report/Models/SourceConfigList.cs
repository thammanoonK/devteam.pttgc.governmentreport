﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Application.Report
{
    public class SourceConfigList
    {
        public IList<SourceConfig> SourceFileConfigs { get; set; }
    }
}
