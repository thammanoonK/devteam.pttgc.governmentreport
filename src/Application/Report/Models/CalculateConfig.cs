﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Application.Report
{
    public class CalculateConfig
    {
        public string ReportName { get; set; }
        public string Type { get; set; }
        public List<CalculateRuleConfig> Source { get; set; }
    }
}
