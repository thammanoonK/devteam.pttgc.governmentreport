﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Application.Report
{
    public class WorkSheetCalculateConfig
    {
        public string Source { get; set; }
        public string Destination { get; set; }
        public int SourceRowStart { get; set; }
        public int DestinationRowStart { get; set; }
        public List<CellCalculateConfig> Cell { get; set; }
    }
}
