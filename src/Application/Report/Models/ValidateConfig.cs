﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Application.Report
{
    public class ValidateConfig
    {
        public string Name { get; set; }
        public bool Required { get; set; }
        public int RowStart { get; set; }
        public List<ValidateRule> Validate { get; set; }
    }
}
