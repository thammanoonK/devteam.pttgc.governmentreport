﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTTGC.GR.Report.Application.Report
{
    public class ReportTypeConfig
    {
        public int Id { get; set; }
        public List<SourceConfig> SourceFileConfigs { get; set; }
        public string TemplateOutput { get; set; }
        public string CalculateFile { get; set; }
        public string DestinationFile { get; set; }
    }
}
