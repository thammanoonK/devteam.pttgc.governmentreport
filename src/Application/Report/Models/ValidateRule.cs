﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Application.Report
{
    public class ValidateRule
    {
        public int Index { get; set; }
        public string Name { get; set; }
        public string DataType { get; set; }
        public bool Required { get; set; }
        public string RowEnd { get; set; }
    }
}
