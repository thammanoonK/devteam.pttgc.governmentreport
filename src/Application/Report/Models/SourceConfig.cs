﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Application.Report
{
    public class SourceConfig
    {
        public string FileName { get; set; }
        public bool IsValid { get; set; }
        public string Description { get; set; }
        public string ValidMessage { get; set; }
        public string ValidateFile { get; set; }
        public byte[] Report { get; set; }
    }
}
