﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Application.Report
{
    public class CalculateRuleConfig
    {
        public string SourceFile { get; set; }
        public List<WorkSheetCalculateConfig> WorkSheet { get; set; }

    }
}
