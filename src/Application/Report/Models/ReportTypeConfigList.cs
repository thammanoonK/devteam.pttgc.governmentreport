﻿using System;
using System.Collections.Generic;
using System.Text;


namespace PTTGC.GR.Report.Application.Report
{
    public class ReportTypeConfigList
    {
        public IList<ReportTypeConfig> ReportConfigs { get; set; }
    }
}
