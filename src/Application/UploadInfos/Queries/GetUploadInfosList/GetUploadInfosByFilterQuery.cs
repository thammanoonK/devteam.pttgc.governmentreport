﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using PTTGC.GR.Report.Application.Common.Exceptions;
using PTTGC.GR.Report.Application.Common.Interfaces;
using PTTGC.GR.Report.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PTTGC.GR.Report.Application.UploadInfos
{
    public class GetUploadInfosByFilterQuery : IRequest<UploadInfosListVm>
    {
        public long Type { get; set; }
        public class GetUploadInfosByFilterQueryHandler : IRequestHandler<GetUploadInfosByFilterQuery, UploadInfosListVm>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetUploadInfosByFilterQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<UploadInfosListVm> Handle(GetUploadInfosByFilterQuery request, CancellationToken cancellationToken)
            {
                var items = await _context.UploadInfos
                     .Where(t => t.ReportTypeId == request.Type)
                    .ProjectTo<UploadInfoDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                var vm = new UploadInfosListVm
                {
                    UploadInfos = items
                };

                return vm;
            }
        }
    }
}
