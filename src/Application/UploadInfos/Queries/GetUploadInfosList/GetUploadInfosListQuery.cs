﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using PTTGC.GR.Report.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace PTTGC.GR.Report.Application.UploadInfos
{
    public class GetUploadInfosListQuery : IRequest<UploadInfosListVm>
    {
        public class GetUploadInfoListQueryHandler : IRequestHandler<GetUploadInfosListQuery, UploadInfosListVm>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetUploadInfoListQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<UploadInfosListVm> Handle(GetUploadInfosListQuery request, CancellationToken cancellationToken)
            {
                var items = await _context.UploadInfos
                    .ProjectTo<UploadInfoDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                var vm = new UploadInfosListVm
                {
                    UploadInfos = items
                };

                return vm;
            }
        }
    }
}
