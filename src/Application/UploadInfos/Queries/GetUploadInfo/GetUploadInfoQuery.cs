﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using PTTGC.GR.Report.Application.Common.Exceptions;
using PTTGC.GR.Report.Application.Common.Interfaces;
using PTTGC.GR.Report.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PTTGC.GR.Report.Application.UploadInfos
{
    public class GetUploadInfoQuery : IRequest<UploadInfoVm>
    {
        public long Id { get; set; }

        public class GetUploadInfoQueryHandler : IRequestHandler<GetUploadInfoQuery, UploadInfoVm>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetUploadInfoQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<UploadInfoVm> Handle(GetUploadInfoQuery request, CancellationToken cancellationToken)
            {
                var vm = await _context.UploadInfos
                    .Where(t => t.Id == request.Id)
                    .ProjectTo<UploadInfoVm>(_mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(cancellationToken);

                if (vm == null)
                {
                    throw new NotFoundException(nameof(UploadInfo), request.Id);
                }

                return vm;
            }
        }
    }
}
