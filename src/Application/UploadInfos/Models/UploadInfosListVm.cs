﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Application.UploadInfos
{
    public class UploadInfosListVm
    {
        public IList<UploadInfoDto> UploadInfos { get; set; }
    }
}
