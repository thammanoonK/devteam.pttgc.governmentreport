﻿using PTTGC.GR.Report.Application.Common.Mappings;
using PTTGC.GR.Report.Domain.Entities;
using System;
using System.Collections.Generic;

namespace PTTGC.GR.Report.Application.UploadInfos
{
    public class UploadInfoVm : IMapFrom<UploadInfo>
    {
        public long Id { get; set; }
        public long ReportTypeId { get; set; }
        public virtual ReportType ReportType { get; set; }
        public DateTime ReportDate { get; set; }
        public DateTime GenerateDate { get; set; }
        public string GenerateBy { get; set; }
        public int Status { get; set; }
        public string Remark { get; set; }
        public virtual ICollection<DocumentInfo> DocumentInfos { get; set; }
    }
}
