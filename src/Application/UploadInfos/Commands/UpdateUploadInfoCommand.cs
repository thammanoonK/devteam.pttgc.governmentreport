﻿using PTTGC.GR.Report.Application.Common.Exceptions;
using PTTGC.GR.Report.Application.Common.Interfaces;
using PTTGC.GR.Report.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace PTTGC.GR.Report.Application.UploadInfos.Commands
{
    public class UpdateUploadInfoCommand : IRequest
    {
        public long Id { get; set; }
        public long ReportTypeId { get; set; }
        public DateTime ReportDate { get; set; }
        public DateTime? GenerateDate { get; set; }
        public string GenerateBy { get; set; }
        public int Status { get; set; }
        public string Remark { get; set; }
        public class UpdateUploadInfoCommandHandler : IRequestHandler<UpdateUploadInfoCommand>
        {
            private readonly IApplicationDbContext _context;

            public UpdateUploadInfoCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(UpdateUploadInfoCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.UploadInfos.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(UploadInfo), request.Id);
                }

                entity.ReportTypeId = request.ReportTypeId;
                entity.ReportDate = request.ReportDate;
                entity.GenerateDate = request.GenerateDate;
                entity.GenerateBy = request.GenerateBy;
                entity.Status = request.Status;
                entity.Remark = request.Remark;

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
