﻿using PTTGC.GR.Report.Application.Common.Interfaces;
using PTTGC.GR.Report.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace PTTGC.GR.Report.Application.UploadInfos.Commands
{

    public class CreateUploadInfoCommand : IRequest<long>
    {
        public long ReportTypeId { get; set; }
        public DateTime ReportDate { get; set; }
        public DateTime? GenerateDate { get; set; }
        public string GenerateBy { get; set; }
        public int Status { get; set; }
        public string Remark { get; set; }
        public class CreateUploadInfoCommandHandler : IRequestHandler<CreateUploadInfoCommand, long>
        {
            private readonly IApplicationDbContext _context;

            public CreateUploadInfoCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<long> Handle(CreateUploadInfoCommand request, CancellationToken cancellationToken)
            {
                var entity = new UploadInfo
                {
                    ReportTypeId = request.ReportTypeId,
                    ReportDate = request.ReportDate,
                    GenerateDate = request.GenerateDate,
                    GenerateBy = request.GenerateBy,
                    Status = request.Status,
                    Remark = request.Remark,
                };

                _context.UploadInfos.Add(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return entity.Id;
            }
        }
    }
}
