﻿using PTTGC.GR.Report.Application.Common.Interfaces;
using PTTGC.GR.Report.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System;
using PTTGC.GR.Report.Application.Common.Exceptions;

namespace PTTGC.GR.Report.Application.UploadInfos.Commands
{
    public class DeleteUploadInfoCommand : IRequest
    {
        public long Id { get; set; }

        public class DeleteUploadInfoCommandHandler : IRequestHandler<DeleteUploadInfoCommand>
        {
            private readonly IApplicationDbContext _context;

            public DeleteUploadInfoCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteUploadInfoCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.UploadInfos.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(UploadInfo), request.Id);
                }

                _context.UploadInfos.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
