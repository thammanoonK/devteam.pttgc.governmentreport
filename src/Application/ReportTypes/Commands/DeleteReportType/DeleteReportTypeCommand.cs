﻿using PTTGC.GR.Report.Application.Common.Exceptions;
using PTTGC.GR.Report.Application.Common.Interfaces;
using PTTGC.GR.Report.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace PTTGC.GR.Report.Application.ReportTypes.Commands.DeleteReportType
{
    public class DeleteReportTypeCommand : IRequest
    {
        public long Id { get; set; }

        public class DeleteReportTypeCommandHandler : IRequestHandler<DeleteReportTypeCommand>
        {
            private readonly IApplicationDbContext _context;

            public DeleteReportTypeCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteReportTypeCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.ReportTypes.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(TodoItem), request.Id);
                }

                _context.ReportTypes.Remove(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
