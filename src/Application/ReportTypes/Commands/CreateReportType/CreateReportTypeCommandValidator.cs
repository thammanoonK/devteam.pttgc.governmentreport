﻿using FluentValidation;

namespace PTTGC.GR.Report.Application.ReportTypes.Commands.CreateReportType
{
    public class CreateReportTypeCommandValidator : AbstractValidator<CreateReportTypeCommand>
    {
        public CreateReportTypeCommandValidator()
        {
            RuleFor(v => v.Name)
                .MaximumLength(200)
                .NotEmpty();
        }
    }
}
