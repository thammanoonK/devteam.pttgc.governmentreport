﻿using PTTGC.GR.Report.Application.Common.Interfaces;
using PTTGC.GR.Report.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace PTTGC.GR.Report.Application.ReportTypes.Commands.CreateReportType
{
    public class CreateReportTypeCommand : IRequest<long>
    {
        public string Name { get; set; }
        public class CreateReportTypeCommandHandler : IRequestHandler<CreateReportTypeCommand, long>
        {
            private readonly IApplicationDbContext _context;

            public CreateReportTypeCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<long> Handle(CreateReportTypeCommand request, CancellationToken cancellationToken)
            {
                var entity = new ReportType
                {
                    Name = request.Name,
                    IsActive = true
                };

                _context.ReportTypes.Add(entity);

                await _context.SaveChangesAsync(cancellationToken);

                return entity.Id;
            }
        }
    }
}
