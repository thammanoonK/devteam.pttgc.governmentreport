﻿using PTTGC.GR.Report.Application.Common.Exceptions;
using PTTGC.GR.Report.Application.Common.Interfaces;
using PTTGC.GR.Report.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace PTTGC.GR.Report.Application.ReportTypes.Commands.UpdateReportType
{
    public class UpdateReportTypeCommand : IRequest
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

        public class UpdateReportTypeCommandHandler : IRequestHandler<UpdateReportTypeCommand>
        {
            private readonly IApplicationDbContext _context;

            public UpdateReportTypeCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(UpdateReportTypeCommand request, CancellationToken cancellationToken)
            {
                var entity = await _context.ReportTypes.FindAsync(request.Id);

                if (entity == null)
                {
                    throw new NotFoundException(nameof(TodoItem), request.Id);
                }

                entity.Name = request.Name;
                entity.IsActive = request.IsActive;

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
