﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using PTTGC.GR.Report.Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace PTTGC.GR.Report.Application.ReportTypes.Queries.GetReportTypesList
{
    public class GetReportTypesListQuery : IRequest<ReportTypesListVm>
    {
        public class GetReportTypeListQueryQueryHandler : IRequestHandler<GetReportTypesListQuery, ReportTypesListVm>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetReportTypeListQueryQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<ReportTypesListVm> Handle(GetReportTypesListQuery request, CancellationToken cancellationToken)
            {
                var items = await _context.ReportTypes
                    .ProjectTo<ReportTypeDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                var vm = new ReportTypesListVm
                {
                    ReportTypes = items
                };

                return vm;
            }
        }
    }
}
