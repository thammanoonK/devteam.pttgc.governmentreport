﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Application.ReportTypes.Queries.GetReportTypesList
{
    public class ReportTypesListVm
    {
        public IList<ReportTypeDto> ReportTypes { get; set; }
    }
}
