﻿using PTTGC.GR.Report.Application.Common.Mappings;
using PTTGC.GR.Report.Domain.Entities;
using System.Collections.Generic;

namespace PTTGC.GR.Report.Application.ReportTypes.Queries.GetReportTypesList
{
    public class ReportTypeDto : IMapFrom<ReportType>
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string IsActive { get; set; }
        public virtual ICollection<UploadInfo> UploadInfos { get; set; }
    }
}
