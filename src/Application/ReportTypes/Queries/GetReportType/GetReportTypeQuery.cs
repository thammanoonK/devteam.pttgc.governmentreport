﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using PTTGC.GR.Report.Application.Common.Exceptions;
using PTTGC.GR.Report.Application.Common.Interfaces;
using PTTGC.GR.Report.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PTTGC.GR.Report.Application.ReportTypes.Queries.GetReportType
{
    public class GetReportTypeQuery : IRequest<ReportTypeVm>
    {
        public long Id { get; set; }

        public class GetReportTypeQueryHandler : IRequestHandler<GetReportTypeQuery, ReportTypeVm>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetReportTypeQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<ReportTypeVm> Handle(GetReportTypeQuery request, CancellationToken cancellationToken)
            {
                var vm = await _context.ReportTypes
                    .Where(t => t.Id == request.Id)
                    .ProjectTo<ReportTypeVm>(_mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(cancellationToken);

                if (vm == null)
                {
                    throw new NotFoundException(nameof(ReportType), request.Id);
                }

                return vm;
            }
        }
    }
}
