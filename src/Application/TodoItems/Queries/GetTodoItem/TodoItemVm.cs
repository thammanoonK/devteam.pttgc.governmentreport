﻿using PTTGC.GR.Report.Application.Common.Mappings;
using PTTGC.GR.Report.Domain.Entities;

namespace PTTGC.GR.Report.Application.TodoItems.Queries.GetTodoItem
{
    public class TodoItemVm : IMapFrom<TodoItem>
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public bool IsComplete { get; set; }
    }
}
