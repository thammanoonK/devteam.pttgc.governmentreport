﻿using AutoMapper;
using PTTGC.GR.Report.Application.Common.Mappings;
using PTTGC.GR.Report.Domain.Entities;

namespace PTTGC.GR.Report.Application.TodoItems.Queries.GetTodoItemsFile
{
    public class TodoItemFileRecord : IMapFrom<TodoItem>
    {
        public string Name { get; set; }

        public bool Done { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<TodoItem, TodoItemFileRecord>()
                .ForMember(d => d.Done, opt => opt.MapFrom(s => s.IsComplete));
        }
    }
}
