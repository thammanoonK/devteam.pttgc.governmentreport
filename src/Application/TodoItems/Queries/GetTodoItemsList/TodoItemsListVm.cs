﻿using System.Collections.Generic;

namespace PTTGC.GR.Report.Application.TodoItems.Queries.GetTodoItemsList
{
    public class TodoItemsListVm
    {
        public IList<TodoItemDto> TodoItems { get; set; }
    }
}
