﻿using PTTGC.GR.Report.Application.Common.Mappings;
using PTTGC.GR.Report.Domain.Entities;

namespace PTTGC.GR.Report.Application.TodoItems.Queries.GetTodoItemsList
{
        public class TodoItemDto : IMapFrom<TodoItem>
        {
            public long Id { get; set; }

            public string Name { get; set; }

            public bool IsComplete { get; set; }
        }
}
