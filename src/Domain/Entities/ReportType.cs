﻿using PTTGC.GR.Report.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Domain.Entities
{
    public class ReportType : AuditableEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<UploadInfo> UploadInfos { get; set; }
    }
}
