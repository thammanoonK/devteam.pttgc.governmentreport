﻿using PTTGC.GR.Report.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Domain.Entities
{
    public class DocumentInfo : AuditableEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public byte[] Data { get; set; }
        public int Status { get; set; }
        public long UploadInfoId { get; set; }
        public virtual UploadInfo UploadInfo { get; set; }
    }
}
