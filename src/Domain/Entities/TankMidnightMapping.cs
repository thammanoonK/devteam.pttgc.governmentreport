﻿using PTTGC.GR.Report.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Domain.Entities
{
    public class TankMidnightMapping : AuditableEntity
    {
        public long Id { get; set; }
        public string Product { get; set; }
        public string Type { get; set; }
    }
}
