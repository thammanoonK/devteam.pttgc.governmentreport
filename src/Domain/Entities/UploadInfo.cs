﻿using PTTGC.GR.Report.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace PTTGC.GR.Report.Domain.Entities
{
    public class UploadInfo : AuditableEntity
    {
        public long Id { get; set; }
        public long ReportTypeId { get; set; }
        public virtual ReportType ReportType { get; set; }
        public DateTime ReportDate { get; set; }
        public DateTime? GenerateDate { get; set; }
        public string GenerateBy { get; set; }
        public int Status { get; set; }
        public string Remark { get; set; }
        public virtual ICollection<DocumentInfo> DocumentInfos { get; set; }
    }
}
