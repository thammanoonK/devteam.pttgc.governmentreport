﻿using PTTGC.GR.Report.Domain.Common;

namespace PTTGC.GR.Report.Domain.Entities
{
    public class TodoItem : AuditableEntity
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public bool IsComplete { get; set; }
    }
}
