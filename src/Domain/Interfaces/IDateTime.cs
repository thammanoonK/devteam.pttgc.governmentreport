﻿using System;

namespace PTTGC.GR.Report.Domain.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
