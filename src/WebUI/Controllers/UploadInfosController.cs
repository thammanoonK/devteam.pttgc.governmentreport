﻿//using PTTGC.GR.Report.Application.UploadInfos.Commands.CreateUploadInfo;
//using PTTGC.GR.Report.Application.UploadInfos.Commands.DeleteUploadInfo;
//using PTTGC.GR.Report.Application.UploadInfos.Commands.UpdateUploadInfo;
using PTTGC.GR.Report.Application.UploadInfos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace PTTGC.GR.Report.WebUI.Controllers
{
    [Authorize]
    public class UploadInfosController : ApiController
    {
        [HttpGet]
        public async Task<ActionResult<UploadInfosListVm>> GetAll()
        {
            return await Mediator.Send(new GetUploadInfosListQuery());
        }
        [HttpGet("ReportType/{type}")]
        public async Task<ActionResult<UploadInfosListVm>> GetByType(int type)
        {
            return await Mediator.Send(new GetUploadInfosByFilterQuery { Type = type });
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<UploadInfoVm>> Get(long id)
        {
            return await Mediator.Send(new GetUploadInfoQuery { Id = id });
        }

        //[HttpPost]
        //public async Task<ActionResult<long>> Create(CreateReportTypeCommand command)
        //{
        //    return await Mediator.Send(command);
        //}

        //[HttpPut("{id}")]
        //public async Task<ActionResult> Update(long id, UpdateReportTypeCommand command)
        //{
        //    if (id != command.Id)
        //    {
        //        return BadRequest();
        //    }

        //    await Mediator.Send(command);

        //    return NoContent();
        //}

        //[HttpDelete("{id}")]
        //public async Task<ActionResult> Delete(long id)
        //{
        //    await Mediator.Send(new DeleteReportTypeCommand { Id = id });

        //    return NoContent();
        //}
    }
}