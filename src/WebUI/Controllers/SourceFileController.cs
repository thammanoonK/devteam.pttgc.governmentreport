﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using PTTGC.GR.Report.Application.Report;
using Microsoft.AspNetCore.Http;

namespace PTTGC.GR.Report.WebUI.Controllers
{
    [Authorize]
    public class SourceFileController : ApiController
    {
        [HttpPost("Validate/{type}"), DisableRequestSizeLimit]
        public async Task<ActionResult<SourceConfigList>> Validate(int type, List<IFormFile> files)
        {
            switch (type)
            {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;
                case 7:
                    break;
                case 8:
                    return await Mediator.Send(new SourceFileValidate { Id = type, files = files });
                    break;
                default:
                    break;
            }
            return null;
        }
        [HttpPost("GenerateReport/{type}"), DisableRequestSizeLimit]
        public async Task<ActionResult<SourceConfig>> GenerateReport(int type, List<IFormFile> files)
        {
            string dateStr = Request.Form["dateStr"].ToString();
            string remark = Request.Form["remark"].ToString();
            switch (type)
            {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;
                case 7:
                    break;
                case 8:
                    return await Mediator.Send(new GenerateReport { Id = type,dateStr = dateStr, files = files });
                    break;
                default:
                    break;
            }
            return null;
        }
    }
}