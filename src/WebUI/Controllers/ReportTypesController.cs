﻿using PTTGC.GR.Report.Application.ReportTypes.Commands.CreateReportType;
using PTTGC.GR.Report.Application.ReportTypes.Commands.DeleteReportType;
using PTTGC.GR.Report.Application.ReportTypes.Commands.UpdateReportType;
using PTTGC.GR.Report.Application.ReportTypes.Queries.GetReportType;
using PTTGC.GR.Report.Application.ReportTypes.Queries.GetReportTypesList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using PTTGC.GR.Report.Application.Report;

namespace PTTGC.GR.Report.WebUI.Controllers
{
    [Authorize]
    public class ReportTypesController : ApiController
    {
        [HttpGet]
        public async Task<ActionResult<ReportTypesListVm>> GetAll()
        {
            return await Mediator.Send(new GetReportTypesListQuery());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ReportTypeVm>> Get(long id)
        {
            return await Mediator.Send(new GetReportTypeQuery { Id = id });
        }

        [HttpPost]
        public async Task<ActionResult<long>> Create(CreateReportTypeCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(long id, UpdateReportTypeCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(long id)
        {
            await Mediator.Send(new DeleteReportTypeCommand { Id = id });

            return NoContent();
        }

        [HttpGet("GetConfig/{id}")]
        public async Task<ActionResult<ReportTypeConfig>> GetConfig(long id)
        {
            return await Mediator.Send(new GetReportTypeConfigQuery { Id = id });
        }

        [HttpGet("GetSourceConfig/{reportId}")]
        public async Task<ActionResult<SourceConfigList>> GetSourceConfig(long reportId)
        {
            return await Mediator.Send(new GetSourceConfigListQuery { Id = reportId });
        }
    }

    
}