import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { Injectable, Inject, Optional, InjectionToken } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';

export const API_BASE_URL = new InjectionToken<string>('API_BASE_URL');

export interface IReportTypeService {
    getAll(): Observable<ReportTypesListVm>;
    create(command: CreateReportTypeCommand): Observable<number>;
    get(id: number): Observable<ReportTypeVm>;
    getConfig(id: number): Observable<ReportTypeConfig>;
    getSourceFileConfig(id: number): Observable<SourceFileConfigList>;
}

@Injectable({
    providedIn: 'root'
})
export class ReportTypeService implements IReportTypeService {
    private http: HttpClient;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
        this.http = http;
        this.baseUrl = baseUrl ? baseUrl : "";
    }

    getAll(): Observable<ReportTypesListVm> {
        let url_ = this.baseUrl + "/api/ReportTypes";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "application/json"
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetAll(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetAll(<any>response_);
                } catch (e) {
                    return <Observable<ReportTypesListVm>><any>_observableThrow(e);
                }
            } else
                return <Observable<ReportTypesListVm>><any>_observableThrow(response_);
        }));
    }

    protected processGetAll(response: HttpResponseBase): Observable<ReportTypesListVm> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ReportTypesListVm.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ReportTypesListVm>(<any>null);
    }

    create(command: CreateReportTypeCommand): Observable<number> {
        let url_ = this.baseUrl + "/api/ReportTypes";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(command);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Accept": "application/json"
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processCreate(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processCreate(<any>response_);
                } catch (e) {
                    return <Observable<number>><any>_observableThrow(e);
                }
            } else
                return <Observable<number>><any>_observableThrow(response_);
        }));
    }

    protected processCreate(response: HttpResponseBase): Observable<number> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = resultData200 !== undefined ? resultData200 : <any>null;
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<number>(<any>null);
    }

    get(id: number): Observable<ReportTypeVm> {
        let url_ = this.baseUrl + "/api/ReportTypes/{id}";
        if (id === undefined || id === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{id}", encodeURIComponent("" + id));
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "application/json"
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGet(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGet(<any>response_);
                } catch (e) {
                    return <Observable<ReportTypeVm>><any>_observableThrow(e);
                }
            } else
                return <Observable<ReportTypeVm>><any>_observableThrow(response_);
        }));
    }

    protected processGet(response: HttpResponseBase): Observable<ReportTypeVm> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ReportTypeVm.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ReportTypeVm>(<any>null);
    }
    //////
    getConfig(id: number): Observable<ReportTypeConfig> {
        let url_ = this.baseUrl + "/api/ReportTypes/GetConfig/{id}";
        if (id === undefined || id === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{id}", encodeURIComponent("" + id));
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "application/json"
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetConfig(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetConfig(<any>response_);
                } catch (e) {
                    return <Observable<ReportTypeConfig>><any>_observableThrow(e);
                }
            } else
                return <Observable<ReportTypeConfig>><any>_observableThrow(response_);
        }));
    }

    protected processGetConfig(response: HttpResponseBase): Observable<ReportTypeConfig> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = ReportTypeConfig.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<ReportTypeConfig>(<any>null);
    }
    ////
    getSourceFileConfig(reportId: number): Observable<SourceFileConfigList> {
        let url_ = this.baseUrl + "/api/ReportTypes/GetSourceConfig/{reportId}";
        if (reportId === undefined || reportId === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{reportId}", encodeURIComponent("" + reportId));
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "application/json"
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetSourceFileConfig(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetSourceFileConfig(<any>response_);
                } catch (e) {
                    return <Observable<SourceFileConfigList>><any>_observableThrow(e);
                }
            } else
                return <Observable<SourceFileConfigList>><any>_observableThrow(response_);
        }));
    }

    protected processGetSourceFileConfig(response: HttpResponseBase): Observable<SourceFileConfigList> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = SourceFileConfigList.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<SourceFileConfigList>(<any>null);
    }
}

export class ReportTypesListVm implements IReportTypesListVm {
    reportTypes?: ReportTypeDto[] | undefined;

    constructor(data?: IReportTypesListVm) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if (Array.isArray(data["reportTypes"])) {
                this.reportTypes = [] as any;
                for (let item of data["reportTypes"])
                    this.reportTypes!.push(ReportTypeDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): ReportTypesListVm {
        data = typeof data === 'object' ? data : {};
        let result = new ReportTypesListVm();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        if (Array.isArray(this.reportTypes)) {
            data["reportTypes"] = [];
            for (let item of this.reportTypes)
                data["reportTypes"].push(item.toJSON());
        }
        return data;
    }
}

export interface IReportTypesListVm {
    reportTypes?: ReportTypeDto[] | undefined;
}

export class ReportTypeDto implements IReportTypeDto {
    id?: number;
    name?: string | undefined;
    isActive?: boolean;

    constructor(data?: IReportTypeDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.name = data["name"];
            this.isActive = data["isActive"];
        }
    }

    static fromJS(data: any): ReportTypeDto {
        data = typeof data === 'object' ? data : {};
        let result = new ReportTypeDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["name"] = this.name;
        data["isActive"] = this.isActive;
        return data;
    }
}

export interface IReportTypeDto {
    id?: number;
    name?: string | undefined;
    isActive?: boolean;
}

export class ReportTypeVm implements IReportTypeVm {
    id?: number;
    name?: string | undefined;
    isActive?: boolean;

    constructor(data?: IReportTypeVm) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.name = data["name"];
            this.isActive = data["isActive"];
        }
    }

    static fromJS(data: any): ReportTypeVm {
        data = typeof data === 'object' ? data : {};
        let result = new ReportTypeVm();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["name"] = this.name;
        data["isActive"] = this.isActive;
        return data;
    }
}

export interface IReportTypeVm {
    id?: number;
    name?: string | undefined;
    isActive?: boolean;
}

export class CreateReportTypeCommand implements ICreateReportTypeCommand {
    name?: string | undefined;

    constructor(data?: ICreateReportTypeCommand) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.name = data["name"];
        }
    }

    static fromJS(data: any): CreateReportTypeCommand {
        data = typeof data === 'object' ? data : {};
        let result = new CreateReportTypeCommand();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["name"] = this.name;
        return data;
    }
}

export interface ICreateReportTypeCommand {
    name?: string | undefined;
}

export class UpdateReportTypeCommand implements IUpdateReportTypeCommand {
    id?: number;
    name?: string | undefined;
    isActive?: boolean;

    constructor(data?: IUpdateReportTypeCommand) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.name = data["name"];
            this.isActive = data["isActive"];
        }
    }

    static fromJS(data: any): UpdateReportTypeCommand {
        data = typeof data === 'object' ? data : {};
        let result = new UpdateReportTypeCommand();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["name"] = this.name;
        data["isActive"] = this.isActive;
        return data;
    }
}

export interface IUpdateReportTypeCommand {
    id?: number;
    name?: string | undefined;
    isActive?: boolean;
}

///////////////
export class ReportTypeConfig implements IReportTypeConfig {
    id?: number;
    templateOutput?: string | undefined;
    calculateFile?: string | undefined;
    destinationFile?: string | undefined;

    constructor(data?: ReportTypeConfig) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.templateOutput = data["templateOutput"];
            this.calculateFile = data["calculateFile"];
            this.destinationFile = data["destinationFile"];
        }
    }

    static fromJS(data: any): ReportTypeConfig {
        data = typeof data === 'object' ? data : {};
        let result = new ReportTypeConfig();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["templateOutput"] = this.templateOutput;
        data["calculateFile"] = this.calculateFile;
        data["destinationFile"] = this.destinationFile;
        return data;
    }
}

export interface IReportTypeConfig {
    id?: number;
    templateOutput?: string | undefined;
    calculateFile?: string | undefined;
    destinationFile?: string | undefined;

}
///////////////
export class SourceFileConfig implements ISourceFileConfig {
    fileName?: string | undefined;
    isValid?: boolean;
    description?: string | undefined;
    validMessage?: string | undefined;
    validateFile?: string | undefined;
    Valid?: boolean;
    InValid?: boolean;
    constructor(data?: SourceFileConfig) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.fileName = data["fileName"];
            this.isValid = data["isValid"];
            this.description = data["description"];
            this.validMessage = data["validMessage"];
            this.validateFile = data["validateFile"];
        }
      
    }

    static fromJS(data: any): SourceFileConfig {
        data = typeof data === 'object' ? data : {};
        let result = new SourceFileConfig();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["fileName"] = this.fileName;
        data["isValid"] = this.isValid;
        data["description"] = this.description;
        data["validMessage"] = this.validMessage;
        data["validateFile"] = this.validateFile;
        return data;
    }
}

export interface ISourceFileConfig {
    fileName?: string | undefined;
    isValid?: boolean;
    description?: string | undefined;
    validMessage?: string | undefined;
    validateFile?: string | undefined;
    Valid?: boolean;
    InValid?: boolean;
}
export class SourceFileConfigList implements ISourceFileConfigList {
    sourceFileConfigs?: SourceFileConfig[] | undefined;

    constructor(data?: ISourceFileConfigList) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if (Array.isArray(data["sourceFileConfigs"])) {
                this.sourceFileConfigs = [] as any;
                for (let item of data["sourceFileConfigs"])
                    this.sourceFileConfigs!.push(SourceFileConfig.fromJS(item));
            }
        }
    }

    static fromJS(data: any): SourceFileConfigList {
        data = typeof data === 'object' ? data : {};
        let result = new SourceFileConfigList();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        if (Array.isArray(this.sourceFileConfigs)) {
            data["sourceFileConfigs"] = [];
            for (let item of this.sourceFileConfigs)
                data["sourceFileConfigs"].push(item.toJSON());
        }
        return data;
    }
}

export interface ISourceFileConfigList {
    sourceFileConfigs?: SourceFileConfig[] | undefined;
}

export class SwaggerException extends Error {
    message: string;
    status: number;
    response: string;
    headers: { [key: string]: any; };
    result: any;

    constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
        super();

        this.message = message;
        this.status = status;
        this.response = response;
        this.headers = headers;
        this.result = result;
    }

    protected isSwaggerException = true;

    static isSwaggerException(obj: any): obj is SwaggerException {
        return obj.isSwaggerException === true;
    }
}

function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): Observable<any> {
    if (result !== null && result !== undefined)
        return _observableThrow(result);
    else
        return _observableThrow(new SwaggerException(message, status, response, headers, null));
}

function blobToText(blob: any): Observable<string> {
    return new Observable<string>((observer: any) => {
        if (!blob) {
            observer.next("");
            observer.complete();
        } else {
            let reader = new FileReader();
            reader.onload = event => {
                observer.next((<any>event.target).result);
                observer.complete();
            };
            reader.readAsText(blob);
        }
    });
}
