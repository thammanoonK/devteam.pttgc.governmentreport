import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportTypeComponent } from './reporttype.component';

describe('ReporttypeComponent', () => {
    let component: ReportTypeComponent;
    let fixture: ComponentFixture<ReportTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [ReportTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(ReportTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
