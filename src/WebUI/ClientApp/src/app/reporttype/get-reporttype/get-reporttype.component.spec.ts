import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetReportTypeComponent } from './get-reporttype.component';

describe('GetReporttypeComponent', () => {
    let component: GetReportTypeComponent;
    let fixture: ComponentFixture<GetReportTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations: [GetReportTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(GetReportTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
