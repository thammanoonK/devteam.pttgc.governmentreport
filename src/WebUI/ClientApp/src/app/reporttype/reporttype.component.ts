import { Component, OnInit, ViewChild } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs/Observable';

import { ReportTypeService, ReportTypesListVm, CreateReportTypeCommand, ReportTypeDto, UpdateReportTypeCommand } from './services/reporttype.service';
 

@Component({
    selector: 'app-reporttype',
    templateUrl: './reporttype.component.html',
    styleUrls: ['./reporttype.component.css']
})
export class ReportTypeComponent implements OnInit {
    vm: ReportTypesListVm;
    _service: ReportTypeService;  
    displayedColumns = ['id', 'name','isActive'];
    ELEMENT_DATA: ReportTypeDto[] = [];
    dataSource = new MatTableDataSource<ReportTypeDto>(this.ELEMENT_DATA);
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    constructor(private service: ReportTypeService) {
        this._service = service;
    }

    ngOnInit() {
        this._service.getAll().subscribe(
            result => {
                this.vm = result;
                console.log(this.vm.reportTypes);
                this.ELEMENT_DATA = this.vm.reportTypes
                this.dataSource = new MatTableDataSource<ReportTypeDto>(this.ELEMENT_DATA);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            },
            error => console.error(error)
        );
       
    }
    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
}
