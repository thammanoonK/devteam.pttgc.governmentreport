import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { ReportTypeService, ReportTypesListVm, CreateReportTypeCommand, ReportTypeDto, UpdateReportTypeCommand } from '../services/reporttype.service';

@Component({
    selector: 'app-add-reporttype',
    templateUrl: './add-reporttype.component.html',
    styleUrls: ['./add-reporttype.component.css']
})
export class AddReportTypeComponent implements OnInit {
    public reporttypeForm: FormGroup;
    constructor(private location: Location,private service: ReportTypeService) {

    }

    ngOnInit() {
        this.reporttypeForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
        });
    }
    public createReportType = (reporttypeFormValue) => {
        if (this.reporttypeForm.valid) {
            this.executeReportTypeCreation(reporttypeFormValue);
        }
    }
    private executeReportTypeCreation = (reporttypeFormValue) => {
        let reportName: string = reporttypeFormValue.name
        var command = CreateReportTypeCommand.fromJS({ name: reportName });
        this.service.create(command).subscribe(
            result => {
                console.log("success");
            },
            error => console.error(error)
        );
    }
    public hasError = (controlName: string, errorName: string) => {
        return this.reporttypeForm.controls[controlName].hasError(errorName);
    }
    public onCancel = () => {
        this.location.back();
    }
    //onSubmit() {
    //    var command = CreateReportTypeCommand.fromJS({ name: this.reporttypeForm.name });
    //    this.service.create(command).subscribe(
    //        result => {
    //            console.log("success");
    //        },
    //        error => console.error(error)
    //    );
    //}
}
