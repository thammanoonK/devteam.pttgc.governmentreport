import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ReportTypeService, ReportTypesListVm, CreateReportTypeCommand, ReportTypeDto, UpdateReportTypeCommand } from '../reporttype/services/reporttype.service';

@Component({
  selector: 'app-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.css']
})
export class AppLayoutComponent implements OnInit {
    vm: ReportTypesListVm = new ReportTypesListVm();
    _service: ReportTypeService;  

    constructor(private breakpointObserver: BreakpointObserver, private service: ReportTypeService) {
        this._service = service;
    }

    ngOnInit() {
        this._service.getAll().subscribe(
            result => {
                this.vm = result;
            },
            error => console.error(error)
        );
    }
    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
        .pipe(
            map(result => result.matches)
        );

}
