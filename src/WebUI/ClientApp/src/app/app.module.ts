import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from './material/material.module';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { TodoComponent } from './todo/todo.component';
import { ApiAuthorizationModule } from 'src/api-authorization/api-authorization.module';
import { AuthorizeGuard } from 'src/api-authorization/authorize.guard';
import { AuthorizeInterceptor } from 'src/api-authorization/authorize.interceptor';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DataService } from './data/data.service';
import { ReportTypeComponent } from './reporttype/reporttype.component';
import { AddReportTypeComponent } from './reporttype/add-reporttype/add-reporttype.component';
import { EditReportTypeComponent } from './reporttype/edit-reporttype/edit-reporttype.component';
import { GetReportTypeComponent } from './reporttype/get-reporttype/get-reporttype.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { UploadInfoComponent } from './upload-info/upload-info.component';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { UploadSourceComponent } from './upload-source/upload-source.component';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        CounterComponent,
        FetchDataComponent,
        TodoComponent,
        DashboardComponent,
        ReportTypeComponent,
        AddReportTypeComponent,
        EditReportTypeComponent,
        GetReportTypeComponent,
        SideNavComponent,
        UploadInfoComponent,
        AppLayoutComponent,
        UploadSourceComponent
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
        BrowserAnimationsModule,
        MaterialModule,
        FontAwesomeModule,
        HttpClientModule,
        FormsModule,
        FlexLayoutModule,
        ApiAuthorizationModule,
        ReactiveFormsModule,
        RouterModule.forRoot([
            {
                path: 'reporttype',
                children: [
                    { path: '', component: ReportTypeComponent, canActivate: [AuthorizeGuard] },
                    { path: 'add', component: AddReportTypeComponent, pathMatch: 'full', canActivate: [AuthorizeGuard] }
                ]
            },
            {
                path: 'uploadinfo',
                component: AppLayoutComponent,
                children: [
                    { path: ':ReportTypeID', component: UploadInfoComponent, canActivate: [AuthorizeGuard] },
                    { path: 'Upload/:ReportTypeID', component: UploadSourceComponent, canActivate: [AuthorizeGuard] },
                ]
            },
            { path: '', component: HomeComponent, pathMatch: 'full' },
            { path: 'counter', component: CounterComponent },
            { path: 'fetch-data', component: FetchDataComponent, canActivate: [AuthorizeGuard] },
            { path: 'todo', component: TodoComponent, canActivate: [AuthorizeGuard] },
            { path: 'dashboard', component: DashboardComponent, canActivate: [AuthorizeGuard] }
        ])
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: AuthorizeInterceptor, multi: true },
        DataService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
