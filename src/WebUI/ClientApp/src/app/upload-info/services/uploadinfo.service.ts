import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { Injectable, Inject, Optional, InjectionToken } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';

export const API_BASE_URL = new InjectionToken<string>('API_BASE_URL');

export interface IUploadInfoService {
    getAll(): Observable<UploadInfosListVm>;
    getByType(type:number): Observable<UploadInfosListVm>;
}

@Injectable({
  providedIn: 'root'
})
export class UploadInfoService implements IUploadInfoService{

    private http: HttpClient;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
        this.http = http;
        this.baseUrl = baseUrl ? baseUrl : "";
    }

    getAll(): Observable<UploadInfosListVm> {
        let url_ = this.baseUrl + "/api/UploadInfos";
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "application/json"
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetAll(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetAll(<any>response_);
                } catch (e) {
                    return <Observable<UploadInfosListVm>><any>_observableThrow(e);
                }
            } else
                return <Observable<UploadInfosListVm>><any>_observableThrow(response_);
        }));
    }

    protected processGetAll(response: HttpResponseBase): Observable<UploadInfosListVm> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = UploadInfosListVm.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<UploadInfosListVm>(<any>null);
    }
    /////////////////////
    getByType(type: number): Observable<UploadInfosListVm> {
        let url_ = this.baseUrl + "/api/UploadInfos/ReportType/{type}";
        if (type === undefined || type === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{type}", encodeURIComponent("" + type));
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "application/json"
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetByType(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetByType(<any>response_);
                } catch (e) {
                    return <Observable<UploadInfosListVm>><any>_observableThrow(e);
                }
            } else
                return <Observable<UploadInfosListVm>><any>_observableThrow(response_);
        }));
    }
    protected processGetByType(response: HttpResponseBase): Observable<UploadInfosListVm> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = UploadInfosListVm.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<UploadInfosListVm>(<any>null);
    }
    ////////////////////
    create(command: CreateUploadInfoCommand): Observable<number> {
        let url_ = this.baseUrl + "/api/UploadInfos";
        url_ = url_.replace(/[?&]$/, "");

        const content_ = JSON.stringify(command);

        let options_: any = {
            body: content_,
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Accept": "application/json"
            })
        };

        return this.http.request("post", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processCreate(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processCreate(<any>response_);
                } catch (e) {
                    return <Observable<number>><any>_observableThrow(e);
                }
            } else
                return <Observable<number>><any>_observableThrow(response_);
        }));
    }

    protected processCreate(response: HttpResponseBase): Observable<number> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };
        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = resultData200 !== undefined ? resultData200 : <any>null;
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<number>(<any>null);
    }
}

export class UploadInfosListVm implements IUploadInfosListVm {
    uploadInfos?: UploadInfoDto[] | undefined;

    constructor(data?: IUploadInfosListVm) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if (Array.isArray(data["uploadInfos"])) {
                this.uploadInfos = [] as any;
                for (let item of data["uploadInfos"])
                    this.uploadInfos!.push(UploadInfoDto.fromJS(item));
            }
        }
    }

    static fromJS(data: any): UploadInfosListVm {
        data = typeof data === 'object' ? data : {};
        let result = new UploadInfosListVm();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        if (Array.isArray(this.uploadInfos)) {
            data["uploadInfos"] = [];
            for (let item of this.uploadInfos)
                data["uploadInfos"].push(item.toJSON());
        }
        return data;
    }
}

export interface IUploadInfosListVm {
    uploadInfos?: UploadInfoDto[] | undefined;
}

export class UploadInfoDto implements IUploadInfoDto {
    id?: number;
    reportTypeId?: number;
    reportDate?: Date;
    generateDate?: Date;
    generateBy: string | undefined;
    status?: number;
    remark: string | undefined;
    created?: Date;
    createdBy: string | undefined;
    lastModified?: Date;
    lastModifiedBy: string | undefined;

    constructor(data?: IUploadInfoDto) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.reportTypeId = data["reportTypeId"];
            this.reportDate = data["reportDate"];
            this.generateDate = data["generateDate"];
            this.generateBy = data["generateBy"];
            this.status = data["status"];
            this.remark = data["remark"];
            this.created = data["created"];
            this.createdBy = data["createdBy"];
            this.lastModified = data["lastModified"];
            this.lastModifiedBy = data["lastModifiedBy"];
        }
    }

    static fromJS(data: any): UploadInfoDto {
        data = typeof data === 'object' ? data : {};
        let result = new UploadInfoDto();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["reportTypeId"] = this.reportTypeId;
        data["reportDate"] = this.reportDate;
        data["generateDate"] = this.generateDate;
        data["generateBy"] = this.generateBy;
        data["status"] = this.status;
        data["remark"] = this.remark;
        data["created"] = this.created;
        data["createdBy"] = this.createdBy;
        data["lastModified"] = this.lastModified;
        data["lastModifiedBy"] = this.lastModifiedBy;
        return data;
    }
}

export interface IUploadInfoDto {
    id?: number;
    reportTypeId?: number;
    reportDate?: Date;
    generateDate?: Date;
    generateBy: string | undefined;
    status?: number;
    remark: string | undefined;
    created?: Date;
    createdBy: string | undefined;
    lastModified?: Date;
    lastModifiedBy: string | undefined;
}

export class UploadInfoVm implements IUploadInfoVm {
    id?: number;
    reportTypeId?: number;
    reportDate?: Date;
    generateDate?: Date;
    generateBy: string | undefined;
    status?: number;
    remark: string | undefined;
    created?: Date;
    createdBy: string | undefined;
    lastModified?: Date;
    lastModifiedBy: string | undefined;

    constructor(data?: IUploadInfoVm) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.reportTypeId = data["reportTypeId"];
            this.reportDate = data["reportDate"];
            this.generateDate = data["generateDate"];
            this.generateBy = data["generateBy"];
            this.status = data["status"];
            this.remark = data["remark"];
            this.created = data["created"];
            this.createdBy = data["createdBy"];
            this.lastModified = data["lastModified"];
            this.lastModifiedBy = data["lastModifiedBy"];
        }
    }

    static fromJS(data: any): UploadInfoVm {
        data = typeof data === 'object' ? data : {};
        let result = new UploadInfoVm();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["reportTypeId"] = this.reportTypeId;
        data["reportDate"] = this.reportDate;
        data["generateDate"] = this.generateDate;
        data["generateBy"] = this.generateBy;
        data["status"] = this.status;
        data["remark"] = this.remark;
        data["created"] = this.created;
        data["createdBy"] = this.createdBy;
        data["lastModified"] = this.lastModified;
        data["lastModifiedBy"] = this.lastModifiedBy;
        return data;
    }
}

export interface IUploadInfoVm {
    id?: number;
    reportTypeId?: number;
    reportDate?: Date;
    generateDate?: Date;
    generateBy: string | undefined;
    status?: number;
    remark: string | undefined;
    created?: Date;
    createdBy: string | undefined;
    lastModified?: Date;
    lastModifiedBy: string | undefined;
}

export class CreateUploadInfoCommand implements ICreateUploadInfoCommand {
    reportTypeId?: number;
    reportDate?: Date;
    generateDate?: Date;
    generateBy: string | undefined;
    status?: number;
    remark: string | undefined;

    constructor(data?: ICreateUploadInfoCommand) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.reportTypeId = data["reportTypeId"];
            this.reportDate = data["reportDate"];
            this.generateDate = data["generateDate"];
            this.generateBy = data["generateBy"];
            this.status = data["status"];
            this.remark = data["remark"];
        }
    }

    static fromJS(data: any): CreateUploadInfoCommand {
        data = typeof data === 'object' ? data : {};
        let result = new CreateUploadInfoCommand();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["reportTypeId"] = this.reportTypeId;
        data["reportDate"] = this.reportDate;
        data["generateDate"] = this.generateDate;
        data["generateBy"] = this.generateBy;
        data["status"] = this.status;
        data["remark"] = this.remark;
        return data;
    }
}

export interface ICreateUploadInfoCommand {
    reportTypeId?: number;
    reportDate?: Date;
    generateDate?: Date;
    generateBy: string | undefined;
    status?: number;
    remark: string | undefined;
}

export class UpdateUploadInfoCommand implements IUpdateUploadInfoCommand {
    id?: number;
    reportTypeId?: number;
    reportDate?: Date;
    generateDate?: Date;
    generateBy: string | undefined;
    status?: number;
    remark: string | undefined;

    constructor(data?: IUpdateUploadInfoCommand) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.id = data["id"];
            this.reportTypeId = data["reportTypeId"];
            this.reportDate = data["reportDate"];
            this.generateDate = data["generateDate"];
            this.generateBy = data["generateBy"];
            this.status = data["status"];
            this.remark = data["remark"];
        }
    }

    static fromJS(data: any): UpdateUploadInfoCommand {
        data = typeof data === 'object' ? data : {};
        let result = new UpdateUploadInfoCommand();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["id"] = this.id;
        data["reportTypeId"] = this.reportTypeId;
        data["reportDate"] = this.reportDate;
        data["generateDate"] = this.generateDate;
        data["generateBy"] = this.generateBy;
        data["status"] = this.status;
        data["remark"] = this.remark;
        return data;
    }
}

export interface IUpdateUploadInfoCommand {
    id?: number;
    reportTypeId?: number;
    reportDate?: Date;
    generateDate?: Date;
    generateBy: string | undefined;
    status?: number;
    remark: string | undefined;
}

export class SwaggerException extends Error {
    message: string;
    status: number;
    response: string;
    headers: { [key: string]: any; };
    result: any;

    constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
        super();

        this.message = message;
        this.status = status;
        this.response = response;
        this.headers = headers;
        this.result = result;
    }

    protected isSwaggerException = true;

    static isSwaggerException(obj: any): obj is SwaggerException {
        return obj.isSwaggerException === true;
    }
}

function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): Observable<any> {
    if (result !== null && result !== undefined)
        return _observableThrow(result);
    else
        return _observableThrow(new SwaggerException(message, status, response, headers, null));
}

function blobToText(blob: any): Observable<string> {
    return new Observable<string>((observer: any) => {
        if (!blob) {
            observer.next("");
            observer.complete();
        } else {
            let reader = new FileReader();
            reader.onload = event => {
                observer.next((<any>event.target).result);
                observer.complete();
            };
            reader.readAsText(blob);
        }
    });
}
