import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataSource } from '@angular/cdk/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs/Observable';

import { UploadInfoService, UploadInfosListVm, CreateUploadInfoCommand, UploadInfoDto, UpdateUploadInfoCommand } from './services/uploadinfo.service';
import { ReportTypeService, ReportTypesListVm, ReportTypeVm } from '../reporttype/services/reporttype.service';

export interface UploadStatus {
    value: number;
    viewValue: string;
}
@Component({
  selector: 'app-upload-info',
  templateUrl: './upload-info.component.html',
  styleUrls: ['./upload-info.component.css']
})
export class UploadInfoComponent implements OnInit {
    reportType: number;
    reportTypeVm: ReportTypeVm = new ReportTypeVm();
    vm: UploadInfosListVm;
    _uploadInfoservice: UploadInfoService;
    _reportTypeservice: ReportTypeService;  
    displayedColumns = ['id', 'created', 'createdBy', 'lastModified', 'lastModifiedBy', 'generateDate', 'generateBy', 'status', 'action'];
    ELEMENT_DATA: UploadInfoDto[] = [];
    dataSource = new MatTableDataSource<UploadInfoDto>(this.ELEMENT_DATA);
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) sort: MatSort;


    StatusList: UploadStatus[]  = [
        { value: 1, viewValue: 'Draft' },
        { value: 2, viewValue: 'Generated' },
        { value: 3, viewValue: 'Completed' }
    ];

    constructor(private _Route: Router, private route: ActivatedRoute, private uploadInfoservice: UploadInfoService, private reportTypeservice: ReportTypeService) {
        this._uploadInfoservice = uploadInfoservice;
        this._reportTypeservice = reportTypeservice;
    }

    ngOnInit() {
        this.route.params.subscribe(routeParams => {
            console.log(routeParams.ReportTypeID);
            this.reportType = routeParams.ReportTypeID;
            this._uploadInfoservice.getByType(this.reportType).subscribe(
                result => {
                    this.vm = result;
                    console.log(this.vm.uploadInfos);
                    this.ELEMENT_DATA = this.vm.uploadInfos
                    this.dataSource = new MatTableDataSource<UploadInfoDto>(this.ELEMENT_DATA);
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                },
                error => console.error(error)
            );
            this._reportTypeservice.get(this.reportType).subscribe(
                result => {
                    this.reportTypeVm = result;

                    console.log(this.reportTypeVm.name);
                },
                error => console.error(error)
            );
        });
        
  }

}
