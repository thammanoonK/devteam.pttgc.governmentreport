import { TestBed } from '@angular/core/testing';

import { SourcefileService } from './sourcefile.service';

describe('SourcefileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SourcefileService = TestBed.get(SourcefileService);
    expect(service).toBeTruthy();
  });
});
