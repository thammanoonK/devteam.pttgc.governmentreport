import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Observable, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { Injectable, Inject, Optional, InjectionToken } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';

export const API_BASE_URL = new InjectionToken<string>('API_BASE_URL');

export interface ISourcefileService {
    getSourceFileConfig(id: number): Observable<SourceFileConfigList>;
    validate(id: number, formdata: FormData): any;
    generateReport(id: number, formdata: FormData): any;
}

@Injectable({
  providedIn: 'root'
})
export class SourcefileService implements ISourcefileService {

    private http: HttpClient;
    private baseUrl: string;
    protected jsonParseReviver: ((key: string, value: any) => any) | undefined = undefined;

    constructor(@Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
        this.http = http;
        this.baseUrl = baseUrl ? baseUrl : "";
    }

    validate(id: number, formdata: FormData) {
        let url_ = this.baseUrl + "/api/SourceFile/Validate/{id}";
        if (id === undefined || id === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{id}", encodeURIComponent("" + id));
        url_ = url_.replace(/[?&]$/, "");

        let headers = new HttpHeaders({ "Accept": 'application/json' });
        return this.http.post<any>(url_, formdata, { headers: headers })
            .pipe(
                 
            );
    }
    generateReport(id: number, formdata: FormData) {
        let url_ = this.baseUrl + "/api/SourceFile/GenerateReport/{id}";
        if (id === undefined || id === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{id}", encodeURIComponent("" + id));
        url_ = url_.replace(/[?&]$/, "");

        let headers = new HttpHeaders({ "Accept": 'application/json' });
        return this.http.post<any>(url_, formdata, { headers: headers })
            .pipe(

            );
    }
    ////////////////
    getSourceFileConfig(reportId: number): Observable<SourceFileConfigList> {
        let url_ = this.baseUrl + "/api/ReportTypes/GetSourceConfig/{reportId}";
        if (reportId === undefined || reportId === null)
            throw new Error("The parameter 'id' must be defined.");
        url_ = url_.replace("{reportId}", encodeURIComponent("" + reportId));
        url_ = url_.replace(/[?&]$/, "");

        let options_: any = {
            observe: "response",
            responseType: "blob",
            headers: new HttpHeaders({
                "Accept": "application/json"
            })
        };

        return this.http.request("get", url_, options_).pipe(_observableMergeMap((response_: any) => {
            return this.processGetSourceFileConfig(response_);
        })).pipe(_observableCatch((response_: any) => {
            if (response_ instanceof HttpResponseBase) {
                try {
                    return this.processGetSourceFileConfig(<any>response_);
                } catch (e) {
                    return <Observable<SourceFileConfigList>><any>_observableThrow(e);
                }
            } else
                return <Observable<SourceFileConfigList>><any>_observableThrow(response_);
        }));
    }

    protected processGetSourceFileConfig(response: HttpResponseBase): Observable<SourceFileConfigList> {
        const status = response.status;
        const responseBlob =
            response instanceof HttpResponse ? response.body :
                (<any>response).error instanceof Blob ? (<any>response).error : undefined;

        let _headers: any = {}; if (response.headers) { for (let key of response.headers.keys()) { _headers[key] = response.headers.get(key); } };

        if (status === 200) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                let result200: any = null;
                let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
                result200 = SourceFileConfigList.fromJS(resultData200);
                return _observableOf(result200);
            }));
        } else if (status !== 200 && status !== 204) {
            return blobToText(responseBlob).pipe(_observableMergeMap(_responseText => {
                return throwException("An unexpected server error occurred.", status, _responseText, _headers);
            }));
        }
        return _observableOf<SourceFileConfigList>(<any>null);
    }
}

export class SourceFileConfig implements ISourceFileConfig {
    fileName?: string | undefined;
    isValid?: boolean;
    description?: string | undefined;
    validMessage?: string | undefined;
    validateFile?: string | undefined;

    constructor(data?: SourceFileConfig) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            this.fileName = data["fileName"];
            this.isValid = data["isValid"];
            this.description = data["description"];
            this.validMessage = data["validMessage"];
            this.validateFile = data["validateFile"];
        }

    }

    static fromJS(data: any): SourceFileConfig {
        data = typeof data === 'object' ? data : {};
        let result = new SourceFileConfig();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        data["fileName"] = this.fileName;
        data["isValid"] = this.isValid;
        data["description"] = this.description;
        data["validMessage"] = this.validMessage;
        data["validateFile"] = this.validateFile;
        return data;
    }
}

export interface ISourceFileConfig {
    fileName?: string | undefined;
    isValid?: boolean;
    description?: string | undefined;
    validMessage?: string | undefined;
    validateFile?: string | undefined;
}
export class SourceFileConfigList implements ISourceFileConfigList {
    sourceFileConfigs?: SourceFileConfig[] | undefined;

    constructor(data?: ISourceFileConfigList) {
        if (data) {
            for (var property in data) {
                if (data.hasOwnProperty(property))
                    (<any>this)[property] = (<any>data)[property];
            }
        }
    }

    init(data?: any) {
        if (data) {
            if (Array.isArray(data["sourceFileConfigs"])) {
                this.sourceFileConfigs = [] as any;
                for (let item of data["sourceFileConfigs"])
                    this.sourceFileConfigs!.push(SourceFileConfig.fromJS(item));
            }
        }
    }

    static fromJS(data: any): SourceFileConfigList {
        data = typeof data === 'object' ? data : {};
        let result = new SourceFileConfigList();
        result.init(data);
        return result;
    }

    toJSON(data?: any) {
        data = typeof data === 'object' ? data : {};
        if (Array.isArray(this.sourceFileConfigs)) {
            data["sourceFileConfigs"] = [];
            for (let item of this.sourceFileConfigs)
                data["sourceFileConfigs"].push(item.toJSON());
        }
        return data;
    }
}

export interface ISourceFileConfigList {
    sourceFileConfigs?: SourceFileConfig[] | undefined;
}

export interface FileResponse {
    data: Blob;
    status: number;
    fileName?: string;
    headers?: { [name: string]: any };
}
export class SwaggerException extends Error {
    message: string;
    status: number;
    response: string;
    headers: { [key: string]: any; };
    result: any;

    constructor(message: string, status: number, response: string, headers: { [key: string]: any; }, result: any) {
        super();

        this.message = message;
        this.status = status;
        this.response = response;
        this.headers = headers;
        this.result = result;
    }

    protected isSwaggerException = true;

    static isSwaggerException(obj: any): obj is SwaggerException {
        return obj.isSwaggerException === true;
    }
}

function throwException(message: string, status: number, response: string, headers: { [key: string]: any; }, result?: any): Observable<any> {
    if (result !== null && result !== undefined)
        return _observableThrow(result);
    else
        return _observableThrow(new SwaggerException(message, status, response, headers, null));
}

function blobToText(blob: any): Observable<string> {
    return new Observable<string>((observer: any) => {
        if (!blob) {
            observer.next("");
            observer.complete();
        } else {
            let reader = new FileReader();
            reader.onload = event => {
                observer.next((<any>event.target).result);
                observer.complete();
            };
            reader.readAsText(blob);
        }
    });
}
