import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataSource } from '@angular/cdk/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs/Observable';

import { ReportTypeService, ReportTypesListVm, ReportTypeVm, SourceFileConfig } from '../reporttype/services/reporttype.service';
import { SourcefileService } from '../upload-source/services/sourcefile.service';

@Component({
    selector: 'app-upload-source',
    templateUrl: './upload-source.component.html',
    styleUrls: ['./upload-source.component.css']
})
export class UploadSourceComponent implements OnInit {
    _reportTypeService: ReportTypeService;
    _sourceFileService: SourcefileService
    reportType: number;
    reportTypeVm: ReportTypeVm = new ReportTypeVm();
    sourceFileConfigs: SourceFileConfig[] = [];
    FileList: FileUploaod[] = [];
    reportDate: Date = new Date();
    remark: string = "";
    public showValidateButton: boolean = true;
    public showGenerateButton: boolean = false;
    constructor(private _Route: Router, private route: ActivatedRoute, private reportTypeService: ReportTypeService, private sourceFileService: SourcefileService) {
        this._reportTypeService = reportTypeService;
        this._sourceFileService = sourceFileService;
    }

    ngOnInit() {
        this.route.params.subscribe(routeParams => {
            console.log(routeParams.ReportTypeID);
            this.reportType = routeParams.ReportTypeID;
            this._reportTypeService.get(this.reportType).subscribe(
                result => {
                    this.reportTypeVm = result;
                },
                error => console.error(error)
            );
            this._reportTypeService.getSourceFileConfig(this.reportType).subscribe(
                result => {
                    console.log(result.sourceFileConfigs);
                    this.sourceFileConfigs = result.sourceFileConfigs;
                    for (var i = 0, len = this.sourceFileConfigs.length; i < len; i++) {
                        this.sourceFileConfigs[i].Valid = false;
                        this.sourceFileConfigs[i].InValid = false;
                        this.FileList.push({
                            "Name": this.sourceFileConfigs[i].fileName,
                            "SourceFile": null,
                            "Valid": false
                        });
                    }
                },
                error => console.error(error)
            );
        });
        
    }
    onFileChange(event) {
        //console.log(event.target.attributes.id.value);
        let obj = this.FileList.filter(function (file) {
            return file.Name == event.target.attributes.id.value;
        });
        if (event.target.files.length > 0) {
            const file = <File>event.target.files[0];
            obj[0].SourceFile = file;
        } else {
            obj[0].SourceFile = null;
        }
    }
    validate() {
        for (var j = 0, index = this.sourceFileConfigs.length; j < index; j++) {
            this.sourceFileConfigs[j].Valid = false;
            this.sourceFileConfigs[j].InValid = false;
        }
        var isValid = true;
        const formData = new FormData();
        for (var i = 0, len = this.FileList.length; i < len; i++) {
            if (this.FileList[i].SourceFile == null) {
                isValid = false;
            } else {
                let fileToUpload = <File>this.FileList[i].SourceFile;
                formData.append('files', fileToUpload, this.FileList[i].Name);
            }
        }
        this._sourceFileService.validate(this.reportType, formData).subscribe(
            result => {
                console.log(result);
                var isvalid = true;
                for (var i = 0, len = result.sourceFileConfigs.length; i < len; i++) {
                    console.log(result.sourceFileConfigs[i].fileName);
                    for (var j = 0, index = this.sourceFileConfigs.length; j < index; j++) {                      
                        if (this.sourceFileConfigs[j].fileName == result.sourceFileConfigs[i].fileName) {
                            this.sourceFileConfigs[j].isValid = result.sourceFileConfigs[i].isValid;
                            if (result.sourceFileConfigs[i].isValid) {
                                this.sourceFileConfigs[j].Valid = true;
                            }
                            else {
                                this.sourceFileConfigs[j].InValid = true;
                            }
                        }

                    }
                    if (result.sourceFileConfigs[i].isValid) {

                    } else {
                        isvalid = false;
                    }
                }
                if (isvalid) {
                    console.log(this.sourceFileConfigs);
                    this.showGenerateButton = true;
                    this.showValidateButton = false;
                }
            },
            error => console.error(error)
        );
    }
    generateReport() {
        const formData = new FormData();
        if (this.reportDate == null) {
            this.reportDate = new Date();
        }
        var paradate = this.reportDate.getFullYear() + "-" + this.reportDate.getMonth() + "-" + this.reportDate.getDate();
        for (var i = 0, len = this.FileList.length; i < len; i++) {
            let fileToUpload = <File>this.FileList[i].SourceFile;
            formData.append('files', fileToUpload, this.FileList[i].Name);
        }
        formData.append('dateStr', paradate);
        formData.append('remark', this.remark);

        this._sourceFileService.generateReport(this.reportType, formData).subscribe(
            data => {
                console.log(data);

            },
            error => console.error(error)
        );
    }
}
export class FileUploaod {
    public Name: string;
    public SourceFile: File;
    public Valid: boolean;
}
