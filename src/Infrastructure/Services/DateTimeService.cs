﻿using PTTGC.GR.Report.Domain.Interfaces;
using System;

namespace PTTGC.GR.Report.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
