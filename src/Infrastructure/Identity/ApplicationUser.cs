﻿using Microsoft.AspNetCore.Identity;

namespace PTTGC.GR.Report.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
